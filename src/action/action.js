export function setvalue(value) {
    return {
        type: 'SET_VALUE',
        payload: value
    };
}