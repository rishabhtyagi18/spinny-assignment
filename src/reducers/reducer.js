const defaultState = {
    currentValue: [],
}

export default function reducer(state = defaultState, action) {
    switch (action.type) {
        case "SET_VALUE" : 
            return {
                ...state,
                currentValue: action.payload
            }
            default: 
                return state
    }
}
