import React from "react";
import "./header.css";
import { connect } from 'react-redux';

function Header(props) {

  return (
    <>
     <div className="container">
         <form 
          className="search"
         >
            <input 
              type="text"
              name="query"
              placeholder="search for an anime, e.g Naruto"
              className="searchInput"
              value={props.inputs}
              onChange={props.onChange}
              />
            <button 
              onClick={props.submit}
            >
              Go
            </button>
         </form>            
     </div>
    </>
  );
}

function mapStateToProps(state) {
  return state
}

function mapDispatchToProps(dispatch) {
  return {
    dispatch: (action) => dispatch(action),
    action: (action) => dispatch(action),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Header);
