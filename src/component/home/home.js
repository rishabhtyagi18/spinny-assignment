import React, { useState } from "react";
import "./css/main.css";
import Header from '../header/header';
import { connect } from 'react-redux';
import { setvalue } from '../../action/action';

function Home(props) {


  const [maxPageCount, setMaxPageCount] = useState(1);
  const [click, setClick] = useState(0);
  const [inputs, setInputs] = useState('');

  
  function searchString(is_new) {
    if(inputs) {
      const query = inputs;
      const limit = 16;
      const page = is_new ? 1 : click + 1;

      setClick(page);

      const apiUrl = `https://api.jikan.moe/v3/search/anime?q=${query}&limit=${limit}&page=${page}`;
    
      fetch(apiUrl)
        .then((res) => res.json())
        .then((respo) => {

          // Setting prev_results to [] if a new query is entered
          const prev_result = is_new ? [] : props.currentValue || [];
          const results = respo.results || [];

          // Settingmax page count from response
          setMaxPageCount(respo.last_page);

          // appending the new result to previous result.
          props.action(setvalue(prev_result.concat(results)));

        })

        .catch((err) => console.error(err))
    }
  };

  return (
    <>
     <div >
        <Header 
          pageNumber={click}
          onChange={(e) => setInputs(e.target.value)}
          submit={e => {
            e.preventDefault();
            searchString(true);
          }}
          inputs ={inputs}
        />
        <div className="homeContainer">
        <div className="paddingContainer">
          <div className="gridContainer">
            {props.currentValue && 
            props.currentValue.filter((i) => i.title).map((item , index) => {
                 
                  return  (
                    <a href={item.url} className="card">
                      <div className="mainContainer">
                        <div className="card_image">
                          <img 
                            src={item.image_url}
                            alt="img"
                            className="image" 
                          />
                        </div>
                        <div className="card_name">
                          <span>{item.title}</span>
                        </div>
                      </div>
                    </a>
                  )
                }) 
          }
          </div>
          {props.currentValue.length > 0 && maxPageCount >= click && 
            <div 
              className="loadMore"
              onClick={() => {
                searchString(false);
              }}
            >
            load more ...
            </div>
          }  
        </div>
        </div>
    </div>
    </>
  );
}

function mapStateToProps(state) {
  return state
}

function mapDispatchToProps(dispatch) {
  return {
    action: (action) => dispatch(action),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);